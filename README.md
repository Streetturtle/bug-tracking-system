# Bug Tracking System

Simple implementation of a bug-tracking-system. It supports some basic functions, like get issues, create issue, change assignee / status of the issue, get issues of the user. The API documentation is available at /swagger-ui.html. Tech stack:
 - Spring Boot
 - H2 / MySQL
 - Swagger
 - JUnit 4 / MockMVC
 - Docker / Docker Compose
 
## How to run locally

Prerequisite:

 - java 11

Clone the repo, then run:

```bash
$ cd ./bug-tracking-system
$ ./mvnw package
$ java -jar ./target/target/bug-tracking-system-1.0.0.jar
```
 
## How to run in a container

Prerequisite:

 - `docker` v.17.06.0+
 - `docker-compose` v.1.24

Clone the repo and then run:

```bash
$ docker-compose up
```

The above command will start: 

 - MySQL service (will be running on port 3306, login: user, password: password)
 - application service (will be running on port 8080)

In both cases the database will be seeded with following users:

| login | password | role |
--- | --- | ---
| admin| admin | ADMIN|
| user1 | user1 | USER|
| user2 | user2 | USER|

and some other data, which represents the following workflow:

![basic-jira-workflow.png](./basic-jira-workflow.png)