insert into user (username, password, role)
values ('admin', '$2y$12$cJyiWDNrz2GXhTk8kDPHSO9q.EYd.nLdgn.DYaU7osY.oJI9TQrLm', 'ROLE_ADMIN'),
       ('user1', '$2y$12$oi7mvy2sxjckfl8q7/f9NOHc5d68PfhTrvM2dihm/1dMoGEWTL5Ey', 'ROLE_USER'),
       ('user2', '$2y$12$1p8/3/pjedyR5XSi/Qd2.eaS9ns8uQyS2jIvYGX74/OCbpFx49duG', 'ROLE_USER');

insert into status (name, description)
values ('OPEN', 'The issue is opened'),
       ('CLOSED', 'The issue is closed'),
       ('RESOLVED', 'The issue is resolved'),
       ('IN PROGRESS', 'The issue is in progress'),
       ('REOPENED', 'The issue is reopened');

insert into issue (id, name, description, status_name, assignee_username)
values (1, 'PROJ-1', 'Description of a ticket', 'OPEN', 'admin'),
       (2, 'PROJ-2', 'Description of a ticket', 'IN PROGRESS', 'user1');

insert into transition (id, from_status, to_status, description, action)
values (1, 'OPEN', 'IN PROGRESS', 'Start Progress', 'log_hello'),
       (2, 'OPEN', 'RESOLVED', 'Resolve Issue', ''),
       (3, 'OPEN', 'CLOSED', 'Close Issue', ''),
       (4, 'IN PROGRESS', 'OPEN', 'Stop Progress', 'log_hello'),
       (5, 'IN PROGRESS', 'CLOSED', 'Close Issue', ''),
       (6, 'IN PROGRESS', 'RESOLVED', 'Resolve Issue', ''),
       (7, 'CLOSED', 'REOPENED', 'Reopen Issue', 'log_hello'),
       (8, 'RESOLVED', 'CLOSED', 'Close Issue', ''),
       (9, 'RESOLVED', 'REOPENED', 'Reopen Issue', 'log_hello'),
       (10, 'REOPENED', 'RESOLVED', 'Resolve Issue', ''),
       (11, 'REOPENED', 'CLOSED', 'Close Issue', ''),
       (12, 'REOPENED', 'IN PROGRESS', 'Start Progress', '');
