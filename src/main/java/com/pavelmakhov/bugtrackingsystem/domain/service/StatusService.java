package com.pavelmakhov.bugtrackingsystem.domain.service;

import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class StatusService {

    private final StatusRepository statusRepository;
    @Value("${initial.status.name:OPEN}")
    private String initialStatusId;

    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    public Status getInitialStatus() {
        return statusRepository.findByName(initialStatusId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    public Optional<Status> getStatusByName(String statusName) {
        return statusRepository.findByName(statusName);
    }
}
