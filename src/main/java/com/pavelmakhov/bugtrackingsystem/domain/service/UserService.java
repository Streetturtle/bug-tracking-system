package com.pavelmakhov.bugtrackingsystem.domain.service;

import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import com.pavelmakhov.bugtrackingsystem.domain.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> getById(Long id) {
        return userRepository.findById(id);
    }

    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
