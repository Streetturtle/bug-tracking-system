package com.pavelmakhov.bugtrackingsystem.domain.service;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import com.pavelmakhov.bugtrackingsystem.domain.repository.IssueRepository;
import com.pavelmakhov.bugtrackingsystem.security.BtsUserPrincipal;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class IssueService {

    private final IssueRepository issueRepository;
    private final TransitionService transitionService;
    private final StatusService statusService;
    private final UserService userService;

    public IssueService(IssueRepository issueRepository, TransitionService transitionService, StatusService statusService, UserService userService) {
        this.issueRepository = issueRepository;
        this.transitionService = transitionService;
        this.statusService = statusService;
        this.userService = userService;
    }

    public List<Issue> getAll() {
        List<Issue> issues = issueRepository.findAll();
        issues.forEach(this::enrichWithAvailableTransitions);

        return issues;
    }

    public Optional<Issue> getById(Long id) {
        Optional<Issue> optionalIssue = issueRepository.findById(id);
        optionalIssue.ifPresent(this::enrichWithAvailableTransitions);

        return optionalIssue;
    }

    public Issue save(Issue issue) {
        issue.setStatus(statusService.getInitialStatus());
        Issue save = issueRepository.save(issue);
        enrichWithAvailableTransitions(save);

        return save;
    }

    public List<Issue> getSelfIssues() {
        User user = ((BtsUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        List<Issue> issues = issueRepository.findByAssignee(user);
        issues.forEach(this::enrichWithAvailableTransitions);

        return issues;
    }

    public void assignIssueTo(Long issueId, String username) {
        User user = Optional.ofNullable(userService.getByUsername(username))
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format("User with username %s not found", username)));

        Issue issue = getById(issueId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format("Issue with id %s not found", issueId)));

        issue.setAssignee(user);
        issueRepository.save(issue);
    }

    public void changeStatusTo(Long issueId, String newStatusName) {

        Issue issue = getById(issueId)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format("Issue with id %s not found", issueId)));

        Status status = statusService.getStatusByName(newStatusName)
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, String.format("Status with name %s not found", newStatusName)));

        issueRepository.save(transitionService.changeTo(issue, status));
    }

    private void enrichWithAvailableTransitions(Issue issue) {
        issue.setAvailableTransitions(transitionService.getAvailableToStatuses(issue.getStatus()));
    }

}
