package com.pavelmakhov.bugtrackingsystem.domain.service;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import com.pavelmakhov.bugtrackingsystem.domain.repository.TransitionRepository;
import com.pavelmakhov.bugtrackingsystem.handler.TransitionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TransitionService {

    private final TransitionHandler transitionHandler;
    private final TransitionRepository transitionRepository;

    public TransitionService(TransitionRepository transitionRepository, TransitionHandler transitionHandler) {
        this.transitionRepository = transitionRepository;
        this.transitionHandler = transitionHandler;
    }

    public List<Transition> getAvailableToStatuses(Status from) {
        return transitionRepository.findAllByFrom(from);
    }

    public List<Transition> getAllTransitions() {
        return transitionRepository.findAll();
    }

    public Issue changeTo(Issue issue, Status status) {

        Transition transitionToMake = issue.getAvailableTransitions().stream().filter(s -> s.getTo().equals(status)).findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.CONFLICT,
                        String.format("Status %s of the issue %s cannot be changed to %s",
                                issue.getStatus().getName(),
                                issue.getId(),
                                status.getName())));

        transitionHandler.performAction(transitionToMake.getAction());

        issue.setStatus(transitionToMake.getTo());
        return issue;
    }

    public Transition save(Transition transition) {
        return transitionRepository.save(transition);
    }

    public void deleteById(Long transitionIdToDelete) {
        transitionRepository.deleteById(transitionIdToDelete);
    }
}
