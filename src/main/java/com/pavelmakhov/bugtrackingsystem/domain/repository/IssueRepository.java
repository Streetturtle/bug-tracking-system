package com.pavelmakhov.bugtrackingsystem.domain.repository;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {

    List<Issue> findByStatusIn(List<String> statuses);

    List<Issue> findByAssignee(User user);

    List<Issue> findByName(String name);

}
