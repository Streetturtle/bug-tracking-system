package com.pavelmakhov.bugtrackingsystem.domain.repository;

import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransitionRepository extends JpaRepository<Transition, Long> {

    List<Transition> findAllByFrom(Status from);

    List<Transition> findByFromNameAndToName(String from, String to);
}