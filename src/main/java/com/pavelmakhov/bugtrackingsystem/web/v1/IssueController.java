package com.pavelmakhov.bugtrackingsystem.web.v1;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.service.IssueService;
import com.pavelmakhov.bugtrackingsystem.security.SecurityService;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.ChangeStatusRequestDTO;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.CreateIssueRequestDTO;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.IssueDTO;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.UpdateAssigneeRequestDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@RestController
@Api(value = "Issue controller", tags = "Issues")
public class IssueController {

    private final SecurityService securityService;
    private final IssueService issueService;

    public IssueController(IssueService issueService, SecurityService securityService) {
        this.issueService = issueService;
        this.securityService = securityService;
    }

    @ApiOperation(value = "Get all issues", tags = {"Issues"})
    @GetMapping("v1/issues")
    public List<IssueDTO> getAllIssues() {
        return issueService.getAll()
                .stream()
                .map(IssueDTO::fromIssue)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get an issue by id", tags = {"Issues"})
    @GetMapping("v1/issues/{issueId}")
    public IssueDTO getIssueById(@PathVariable String issueId) {
        Issue issue = issueService.getById(Long.valueOf(issueId))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return IssueDTO.fromIssue(issue);
    }

    @ApiOperation(value = "Create an issue", tags = {"Issues"})
    @PostMapping("v1/issues")
    public IssueDTO createIssue(@Valid @RequestBody CreateIssueRequestDTO issueDTO) {
        return IssueDTO.fromIssue(issueService.save(issueDTO.toIssue()));
    }

    @ApiOperation(value = "Get all issues assigned to the logged in user", tags = {"Issues"})
    @GetMapping("v1/self/issues")
    public List<IssueDTO> getSelfIssues() {
        return issueService.getSelfIssues().stream()
                .map(IssueDTO::fromIssue)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Change the assignee of the issue", tags = {"Issues"})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Transition has been created successfully"),
            @ApiResponse(code = 403, message = "User performing an operation is not an admin, or user is assigning an issue not to himself"),
            @ApiResponse(code = 404, message = "User or issue is not found"),
    })
    @PutMapping("v1/issues/{issueId}/assignee")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void assignIssue(@PathVariable String issueId, @RequestBody UpdateAssigneeRequestDTO assignee) {
        String currentUserName = securityService.getCurrentUserName();
        String assigneeUserName = assignee.getUsername();
        if (Objects.equals(currentUserName, assigneeUserName)
                || securityService.isCurrentUserAdmin())
            issueService.assignIssueTo(Long.valueOf(issueId), assigneeUserName);
        else {
            log.error("User {} is not allowed to assign the issue {} to the {}", currentUserName, issueId, assigneeUserName);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User is not allowed to assign an issue");
        }
    }

    @ApiOperation(value = "Change the status of the issue", tags = {"Issues"})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Status of the issue has been changed successfully"),
            @ApiResponse(code = 409, message = "Status of the issue cannot be changed to a given status"),
    })
    @PutMapping("v1/issues/{issueId}/status")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changeStatus(@PathVariable String issueId, @RequestBody ChangeStatusRequestDTO status) {
        issueService.changeStatusTo(Long.valueOf(issueId), status.getNewStatusName());
    }
}
