package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransitionDTO {

    private Long id;
    private StatusDTO from;
    private StatusDTO to;
    private String description;
    private String actionName;

    public static TransitionDTO from(Transition transition) {

        return TransitionDTO.builder()
                .id(transition.getId())
                .from(StatusDTO.fromStatus(transition.getFrom()))
                .to(StatusDTO.fromStatus(transition.getTo()))
                .description(transition.getDescription())
                .actionName(transition.getAction())
                .build();
    }
}
