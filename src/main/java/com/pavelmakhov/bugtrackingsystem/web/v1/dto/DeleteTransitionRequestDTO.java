package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import lombok.Data;

@Data
public class DeleteTransitionRequestDTO {
    private Long transitionIdToDelete;
}
