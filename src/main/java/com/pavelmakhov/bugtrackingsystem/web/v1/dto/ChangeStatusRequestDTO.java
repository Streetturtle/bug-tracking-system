package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import lombok.Data;

@Data
public class ChangeStatusRequestDTO {
    private String newStatusName;
}
