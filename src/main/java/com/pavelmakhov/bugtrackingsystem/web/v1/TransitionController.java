package com.pavelmakhov.bugtrackingsystem.web.v1;

import com.pavelmakhov.bugtrackingsystem.domain.service.TransitionService;
import com.pavelmakhov.bugtrackingsystem.security.SecurityService;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.CreateTransitionRequestDTO;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.DeleteTransitionRequestDTO;
import com.pavelmakhov.bugtrackingsystem.web.v1.dto.TransitionDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(value = "Transition controller", tags = "Transitions")
public class TransitionController {

    private final TransitionService transitionService;
    private final SecurityService securityService;

    public TransitionController(TransitionService transitionService, SecurityService securityService) {
        this.transitionService = transitionService;
        this.securityService = securityService;
    }

    @ApiOperation(value = "Get all transitions", tags = {"Transitions"})
    @GetMapping("/v1/transitions")
    public List<TransitionDTO> getAll() {
        return transitionService.getAllTransitions().stream()
                .map(TransitionDTO::from)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create a transition / Required role: ADMIN", tags = {"Transitions"})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Transition is successfully created."),
            @ApiResponse(code = 403, message = "User performing an operation is not an admin."),
    })
    @PostMapping("/v1/transitions")
    public TransitionDTO createTransition(@RequestBody @Valid CreateTransitionRequestDTO requestDTO) {
        if (securityService.isCurrentUserAdmin()) {
            return TransitionDTO.from(transitionService.save(requestDTO.toTransition()));
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only admins can create a transition");
        }
    }

    @ApiOperation(value = "Delete a transition / Required role: ADMIN", tags = {"Transitions"})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Transition is successfully Deleted."),
            @ApiResponse(code = 403, message = "User performing an operation is not an admin."),
    })
    @DeleteMapping("/v1/transitions")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTransition(@RequestBody @Valid DeleteTransitionRequestDTO requestDTO) {
        if (securityService.isCurrentUserAdmin()) {
            transitionService.deleteById(requestDTO.getTransitionIdToDelete());
        } else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only admins can delete a transition");
        }
    }
}
