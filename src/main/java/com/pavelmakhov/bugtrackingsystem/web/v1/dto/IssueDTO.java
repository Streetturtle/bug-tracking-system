package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@ApiModel(value = "Issue", description = "Issue")
public class IssueDTO {

    List<StatusDTO> availableTransitions;
    @ApiModelProperty("Id of the issue")
    private Long id;
    @ApiModelProperty("Name of the issue")
    private String name;
    @ApiModelProperty("Current status of the issue")
    private StatusDTO status;
    @ApiModelProperty("The user")
    private UserDTO assignee;

    public static IssueDTO fromIssue(Issue issue) {

        IssueDTOBuilder issueDTOBuilder = IssueDTO.builder()
                .id(issue.getId())
                .name(issue.getName())
                .status(StatusDTO.fromStatus(issue.getStatus()))
                .assignee(UserDTO.builder()
                        .username(issue.getAssignee() == null
                                ? null
                                : issue.getAssignee().getUsername())
                        .build());

        if (issue.getAvailableTransitions() != null)
            issueDTOBuilder.availableTransitions(issue.getAvailableTransitions().stream()
                    .map(Transition::getTo)
                    .map(StatusDTO::fromStatus)
                    .collect(Collectors.toList()));

        return issueDTOBuilder.build();
    }

}
