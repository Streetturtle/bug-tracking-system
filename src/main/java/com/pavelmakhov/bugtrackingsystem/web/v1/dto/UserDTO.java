package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Builder
public class UserDTO {

    private String username;
    private List<IssueDTO> issues;

    public static UserDTO fromUser(User user) {

        if (user == null) return null;

        UserDTOBuilder issuesBuilder = UserDTO.builder().username(user.getUsername());

        Optional.ofNullable(user.getIssue())
                .ifPresent(issues -> issuesBuilder
                        .issues(issues.stream()
                                .map(issue -> IssueDTO.builder()
                                        .id(issue.getId())
                                        .name(issue.getName())
                                        .availableTransitions(issue.getAvailableTransitions()
                                                .stream()
                                                .map(Transition::getTo)
                                                .map(StatusDTO::fromStatus)
                                                .collect(Collectors
                                                        .toList()))
                                        .build())
                                .collect(Collectors.toList())));

        return issuesBuilder.build();
    }
}
