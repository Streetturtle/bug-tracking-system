package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateIssueRequestDTO {

    @NotEmpty
    private String name;
    private String description;

    public Issue toIssue() {
        return Issue.builder().name(this.name).description(this.description).build();
    }
}
