package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateTransitionRequestDTO {

    @NotEmpty
    private String fromStatusWithName;
    @NotEmpty
    private String toStatusWithName;
    @NotEmpty
    private String description;
    private String actionName;

    public Transition toTransition() {
        return Transition.builder()
                .from(Status.builder().name(fromStatusWithName).build())
                .to(Status.builder().name(toStatusWithName).build())
                .description(description)
                .action(actionName)
                .build();
    }
}
