package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatusDTO {
    private String name;
    private String description;

    public static StatusDTO fromStatus(Status status) {
        if (status == null) return null;
        return StatusDTO.builder()
                .name(status.getName())
                .description(status.getDescription())
                .build();

    }

    public Status toStatus() {
        return Status.builder()
                .name(name)
                .description(description)
                .build();
    }
}
