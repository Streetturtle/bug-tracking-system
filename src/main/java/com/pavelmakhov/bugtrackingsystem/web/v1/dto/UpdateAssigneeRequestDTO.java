package com.pavelmakhov.bugtrackingsystem.web.v1.dto;

import lombok.Data;

@Data
public class UpdateAssigneeRequestDTO {

    //	private Long id;
    private String username;
}
