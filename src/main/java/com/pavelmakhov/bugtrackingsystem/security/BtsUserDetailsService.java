package com.pavelmakhov.bugtrackingsystem.security;

import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import com.pavelmakhov.bugtrackingsystem.domain.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class BtsUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public BtsUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return new BtsUserPrincipal(user);
    }
}