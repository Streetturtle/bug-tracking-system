package com.pavelmakhov.bugtrackingsystem.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

    public String getCurrentUserName() {
        return getPrincipal().getUsername();
    }

    public boolean isCurrentUserAdmin() {
        return getPrincipal().getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .anyMatch(b -> b.equals("ROLE_ADMIN"));
    }

    private UserDetails getPrincipal() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return (UserDetails) auth.getPrincipal();
    }
}
