package com.pavelmakhov.bugtrackingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class BugTrackingSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BugTrackingSystemApplication.class, args);
    }

}
