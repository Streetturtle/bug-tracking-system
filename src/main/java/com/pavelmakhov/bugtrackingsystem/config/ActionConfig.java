package com.pavelmakhov.bugtrackingsystem.config;

import com.pavelmakhov.bugtrackingsystem.handler.LogAction;
import com.pavelmakhov.bugtrackingsystem.handler.TransitionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Configuration for the action feature which allows to run an action when a transitions happens.
 * In order to register a new action create a class which implements {@link com.pavelmakhov.bugtrackingsystem.handler.Action}
 * and register it in transitionHandler bean's registry, with an action name string key.
 */
@Configuration
public class ActionConfig {

    @Bean
    public TransitionHandler transitionHandler() {
        TransitionHandler handler = new TransitionHandler();

        handler.register("log_hello", new LogAction());

        return handler;
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("Action-");
        executor.initialize();
        return executor;
    }

}
