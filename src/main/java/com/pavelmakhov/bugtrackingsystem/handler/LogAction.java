package com.pavelmakhov.bugtrackingsystem.handler;

import lombok.extern.slf4j.Slf4j;

/**
 * Action which takes 5 seconds to log a message
 */
@Slf4j
public class LogAction implements Action {

    @Override
    public void perform() {
        log.info("Performing action");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Action performed");

    }
}
