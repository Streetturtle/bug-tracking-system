package com.pavelmakhov.bugtrackingsystem.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class TransitionHandler {

    private Map<String, Action> registry;

    public TransitionHandler() {
        registry = new HashMap<>();
    }

    public void register(String actionName, Action action) {
        registry.put(actionName, action);
    }

    @Async
    public void performAction(String actionName) {
        if (registry.containsKey(actionName)) {
            log.info("Action found");
            registry.get(actionName).perform();
        } else {
            log.warn("No action found with name {}", actionName);
        }
    }
}
