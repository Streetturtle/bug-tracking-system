package com.pavelmakhov.bugtrackingsystem.handler;

public interface Action {

    void perform();
}
