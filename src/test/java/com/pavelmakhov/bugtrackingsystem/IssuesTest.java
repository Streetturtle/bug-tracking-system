package com.pavelmakhov.bugtrackingsystem;

import com.pavelmakhov.bugtrackingsystem.domain.model.Issue;
import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import com.pavelmakhov.bugtrackingsystem.domain.model.User;
import com.pavelmakhov.bugtrackingsystem.domain.repository.IssueRepository;
import com.pavelmakhov.bugtrackingsystem.domain.repository.StatusRepository;
import com.pavelmakhov.bugtrackingsystem.domain.repository.TransitionRepository;
import com.pavelmakhov.bugtrackingsystem.domain.repository.UserRepository;
import com.pavelmakhov.bugtrackingsystem.domain.service.IssueService;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IssuesTest {

    @Autowired
    IssueRepository issueRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    TransitionRepository transitionRepository;
    @Autowired
    IssueService issueService;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private Issue issueOne;
    private Issue issueTwo;

    private Status open;
    private Status closed;
    private Status dummyStatus;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        open = statusRepository.save(Status.builder().name("OPEN").build());
        closed = statusRepository.save(Status.builder().name("CLOSED").build());
        dummyStatus = statusRepository.save(Status.builder().name("DUMMY_STATUS").build());

        transitionRepository.save(Transition.builder().from(open).to(closed).build());

        issueOne = issueRepository.save(Issue.builder().name("one").status(open).build());
        issueTwo = issueRepository.save(Issue.builder().name("two").build());

        userRepository.save(User.builder().username("some-username").build());
    }

    @After
    public void tearDown() {
        issueRepository.deleteAll();
        userRepository.deleteAll();
        statusRepository.deleteAll();
        transitionRepository.deleteAll();
    }

    @Test
    public void testGetAll_ok() throws Exception {

        long expected = issueRepository.count();

        mockMvc.perform(get("/v1/issues"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize((int) expected)));
    }

    @Test
    public void testGetById_ok() throws Exception {

        Long id = issueRepository.findAll().get(0).getId();

        mockMvc.perform(get("/v1/issues/" + id))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testGetById_notFound() throws Exception {

        mockMvc.perform(get("/v1/issues/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "ROLE_ADMIN")
    public void testAssignIssueToNonExistingUser_notFound() throws Exception {

        JSONObject request = new JSONObject();
        request.put("username", "non-existing-username");

        mockMvc.perform(put("/v1/issues/1/assignee").content(request.toString()).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(status().reason("User with username non-existing-username not found"));
    }

    @Test
    @WithMockUser(authorities = "ROLE_ADMIN")
    public void testAssignNonExistingIssueToUser_notFound() throws Exception {

        JSONObject request = new JSONObject();
        request.put("username", "some-username");

        mockMvc.perform(put("/v1/issues/100/assignee").content(request.toString()).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(status().reason("Issue with id 100 not found"));
    }

    @Test
    @WithMockUser(authorities = "ROLE_USER")
    public void testAssignIssueToSomebodyAsUser_forbidden() throws Exception {
        {
            JSONObject request = new JSONObject();
            request.put("username", "some-username");

            mockMvc.perform(put("/v1/issues/" + issueOne.getId() + "/assignee").content(request.toString())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isForbidden())
                    .andExpect(status().reason("User is not allowed to assign an issue"));
        }
    }

    @Test
    @WithMockUser(authorities = "ROLE_ADMIN")
    public void testAssignIssueToSomebodyAsAdmin_ok() throws Exception {
        {
            JSONObject request = new JSONObject();
            request.put("username", "some-username");

            mockMvc.perform(put("/v1/issues/" + issueOne.getId() + "/assignee").content(request.toString())
                    .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isNoContent());

            issueRepository.findById(issueOne.getId()).ifPresentOrElse(
                    actualIssue -> assertEquals(actualIssue.getAssignee().getUsername(), "some-username"),
                    Assert::fail);
        }
    }

    @Test
    public void testChangeToWrongStatus_conflict() throws Exception {
        JSONObject request = new JSONObject();
        request.put("newStatusName", dummyStatus.getName());

        mockMvc.perform(put("/v1/issues/" + issueOne.getId() + "/status").content(request.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(status().reason("Status OPEN of the issue " + issueOne.getId() + " cannot be changed to DUMMY_STATUS"));

    }

    @Test
    public void testChangeToRightStatus_ok() throws Exception {
        JSONObject request = new JSONObject();
        request.put("newStatusName", closed.getName());

        mockMvc.perform(put("/v1/issues/" + issueOne.getId() + "/status").content(request.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());

        issueRepository.findById(issueOne.getId()).ifPresentOrElse(
                actualIssue -> assertEquals(actualIssue.getStatus().getName(), closed.getName()),
                Assert::fail);

    }

    @Test
    public void testCreateIssueWithNoName_badRequest() throws Exception {
        JSONObject request = new JSONObject();
        request.put("description", "some description");

        mockMvc.perform(post("/v1/issues/")
                .content(request.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateIssue_ok() throws Exception {
        JSONObject request = new JSONObject();
        request.put("name", "some name");
        request.put("description", "some description");

        mockMvc.perform(post("/v1/issues/")
                .content(request.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Issue> issues = issueRepository.findByName("some name");
        assertThat(issues.size()).isEqualTo(1);
        assertThat(issues.get(0).getName()).isEqualTo("some name");
        assertThat(issues.get(0).getDescription()).isEqualTo("some description");

    }
}
