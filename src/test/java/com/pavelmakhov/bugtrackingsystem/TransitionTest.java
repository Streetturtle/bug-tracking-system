package com.pavelmakhov.bugtrackingsystem;

import com.pavelmakhov.bugtrackingsystem.domain.model.Status;
import com.pavelmakhov.bugtrackingsystem.domain.model.Transition;
import com.pavelmakhov.bugtrackingsystem.domain.repository.StatusRepository;
import com.pavelmakhov.bugtrackingsystem.domain.repository.TransitionRepository;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransitionTest {

    @Autowired
    StatusRepository statusRepository;
    @Autowired
    TransitionRepository transitionRepository;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private Status open;
    private Status closed;
    private Status dummyStatus;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        open = statusRepository.save(Status.builder().name("OPEN").build());
        closed = statusRepository.save(Status.builder().name("CLOSED").build());
        dummyStatus = statusRepository.save(Status.builder().name("DUMMY_STATUS").build());
    }

    @After
    public void tearDown() {
        statusRepository.deleteAll();
        transitionRepository.deleteAll();
    }

    @Test
    @Rollback
    @WithMockUser(authorities = "ROLE_ADMIN")
    public void testCreateTransitionAsAdmin_ok() throws Exception {

        JSONObject request = new JSONObject();
        request.put("fromStatusWithName", "OPEN");
        request.put("toStatusWithName", "CLOSED");
        request.put("description", "description for test");

        mockMvc.perform(
                post("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isOk());

        List<Transition> createdTransitions = transitionRepository.findByFromNameAndToName("OPEN", "CLOSED");

        assertThat(createdTransitions.size()).isEqualTo(1);
        assertThat(createdTransitions.get(0).getFrom().getName()).isEqualTo("OPEN");
        assertThat(createdTransitions.get(0).getTo().getName()).isEqualTo("CLOSED");
    }

    @Test
    @WithMockUser(authorities = "ROLE_USER")
    public void testCreateTransitionAsUser_forbidden() throws Exception {

        JSONObject request = new JSONObject();
        request.put("fromStatusWithName", "OPEN");
        request.put("toStatusWithName", "CLOSED");
        request.put("description", "CLOSED");

        mockMvc.perform(
                post("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason("Only admins can create a transition"));
    }

    @Test
    public void testCreateTransitionWithMissingDescription_badRequest() throws Exception {

        JSONObject request = new JSONObject();
        request.put("fromStatusWithName", "OPEN");
        request.put("toStatusWithName", "CLOSED");

        mockMvc.perform(
                post("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTransitionWithMissingFrom_badRequest() throws Exception {

        JSONObject request = new JSONObject();
        request.put("toStatusWithName", "CLOSED");
        request.put("description", "CLOSED");

        mockMvc.perform(
                post("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateTransitionWithMissingTo_badRequest() throws Exception {

        JSONObject request = new JSONObject();
        request.put("fromStatusWithName", "OPEN");
        request.put("description", "CLOSED");

        mockMvc.perform(
                post("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = "ROLE_ADMIN")
    public void testDeleteTransitionAsAdmin_noContent() throws Exception {

        Transition savedTransition = transitionRepository
                .save(Transition.builder().from(dummyStatus).to(closed).description("test").build());

        JSONObject request = new JSONObject();
        request.put("transitionIdToDelete", savedTransition.getId());
        request.put("description", "CLOSED");

        mockMvc.perform(
                delete("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isNoContent());

        Optional<Transition> one = transitionRepository.findById(savedTransition.getId());
        assertThat(one).isNotPresent();
    }

    @Test
    @WithMockUser(authorities = "ROLE_USER")
    public void testDeleteTransitionAsUser_forbidden() throws Exception {

        JSONObject request = new JSONObject();
        request.put("transitionIdToDelete", "1");
        request.put("description", "CLOSED");

        mockMvc.perform(
                delete("/v1/transitions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request.toString()))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason("Only admins can delete a transition"));
    }


}
